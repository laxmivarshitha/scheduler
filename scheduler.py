import tkinter as tk
from tkinter import messagebox
from tkcalendar import DateEntry
import heapq
from datetime import datetime, timedelta

class TaskScheduler:
    def __init__(self):
        self.task_queue = []

    def add_task(self, task, priority, description="", deadline_date=None):
        current_time = datetime.now()
        deadline_date = deadline_date if deadline_date else current_time.date()
        heapq.heappush(self.task_queue, (priority, task, description, current_time, deadline_date))

    def delete_task(self, task):
        self.task_queue = [(p, t, desc, time, d) for p, t, desc, time, d in self.task_queue if t != task]
        heapq.heapify(self.task_queue)

    def get_tasks_for_date(self, date):
        return [(p, t, desc) for p, t, desc, time, d in self.task_queue if d == date]

    def task_queue_between_dates(self, start_date, end_date):
        return [(p, t, desc) for p, t, desc, time, d in self.task_queue if start_date <= d <= end_date]

class IntroductionWindow:
    def __init__(self, master):
        self.master = master
        self.master.title("Welcome to Task Scheduler")
        self.master.attributes('-fullscreen', True)
        self.master.configure(bg = "#00072D")

        # Load the background image (subsample values adjusted for better visibility)
        bg = tk.PhotoImage(file=r"C:\Users\Laxmi Varshitha\OneDrive\Desktop\Scheduler\ds_pic1.png")
        #bg = bg.subsample(1,1)
        imb = tk.Label(self.master, image=bg,bg = "#00072D")
        imb.image = bg
        imb.place(x = 10, y = 10)
        lb1 = tk.Label(self.master, text = "TASK SCHEDULER",bg = "#00072D",fg ="#00FFFF", font = ("Helvetica",40))
        lb1.place(x = 650, y = 300)

        self.play_button = tk.Button(self.master, text="Plan Your Day", command=self.open_main_window, font=("Helvetica", 20),bg ="#00072D",fg ="#00FFFF")
        self.play_button.place(x=800, y=450)

        self.exit_button = tk.Button(self.master, text="X", command=self.exit_program, font=("Helvetica", 20),bg ="#00072D",fg ="#00FFFF")
        self.exit_button.place(x=1240, y=0)

        self.left_frame = tk.Frame(master)
        self.left_frame.grid(row=0, column=0, padx=10, pady=10)

    def open_main_window(self):
        self.master.destroy()
        root = tk.Tk()
        app = TaskSchedulerUI(root)
        root.mainloop()

    def exit_program(self):
        self.master.destroy()

class ViewTasksWindow:
    def __init__(self, master, scheduler):
        self.master = master
        self.master.title("View Tasks")
        self.master.attributes('-fullscreen', True)
        self.scheduler = scheduler
        self.master.configure(bg = "#00072D")

        # Frame to display tasks for the selected date
        self.tasks_frame = tk.Frame(self.master,bg = "#00072D")
        self.tasks_frame.grid(row=0, column=1, padx=500, pady=10)

        # Full Calendar to display all dates
        self.full_calendar_frame = tk.Frame(self.master,bg = "#00072D")
        self.full_calendar_frame.grid(row=0, column=0, padx=10, pady=10)

        # Full Calendar to display all dates
        self.full_calendar = DateEntry(self.full_calendar_frame, width=12, background='darkblue', foreground='white', borderwidth=2, date_pattern='y-mm-dd')
        self.full_calendar.grid(row=0, column=0, padx=5, pady=5)
        self.full_calendar.bind("<ButtonRelease-1>", self.display_tasks_for_date)
        self.exit_button = tk.Button(self.master, text="X", command=self.exit_program, font=("Helvetica", 20),bg ="#00072D",fg ="#00FFFF")
        self.exit_button.place(x=1240, y=0)

        # Frame to display weekly tasks
        self.weekly_tasks_frame = tk.Frame(self.master,bg = "#00072D")
        self.weekly_tasks_frame.grid(row=1, column=1, padx=10, pady=10)

        # Frame to display monthly tasks
        self.monthly_tasks_frame = tk.Frame(self.master,bg = "#00072D")
        self.monthly_tasks_frame.grid(row=2, column=1, padx=10, pady=10)
        self.view_tasks()
    def exit_program(self):
        self.master.destroy()

    def view_tasks(self):
        # Display tasks for the current date initially
        today = datetime.now().date()
        self.full_calendar.set_date(today)
        self.display_tasks_for_date(None)

        # Display tasks for the present week
        self.display_weekly_tasks()

        # Display tasks for the present month
        self.display_monthly_tasks()

    def display_tasks_for_date(self, event):
        selected_date = self.full_calendar.get_date()
        tasks_for_date = self.scheduler.get_tasks_for_date(selected_date)

        # Clear previous daily tasks
        for widget in self.tasks_frame.winfo_children():
            widget.destroy()

        # Display "TODAY'S TASKS" label
        today_tasks_label = tk.Label(self.tasks_frame, text="TODAY'S TASKS", font=("Helvetica", 20, "bold"),bg ="#00072D",fg ="#00FFFF")
        today_tasks_label.pack(pady=5)

        if tasks_for_date:
            # Display daily tasks as buttons in the frame
            for p, t, desc in tasks_for_date:
                task_button = tk.Button(self.tasks_frame, text=f"Task: {t} | Priority: {p} | Description: {desc}",bg ="#00072D",fg ="#00FFFF",font=("Helvetica", 18))
                task_button.pack(pady=5)
        else:
            # Display "No Tasks" label for daily tasks
            no_tasks_label = tk.Label(self.tasks_frame, text="No tasks for the selected date.",font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
            no_tasks_label.pack(pady=5)

    def display_weekly_tasks(self):
        selected_date = self.full_calendar.get_date()
        today = datetime.now().date()
        start_of_week = today - timedelta(days=today.weekday())
        end_of_week = start_of_week + timedelta(days=6)

        weekly_tasks = self.scheduler.task_queue_between_dates(start_of_week, end_of_week)

        # Clear previous weekly tasks
        for widget in self.weekly_tasks_frame.winfo_children():
            widget.destroy()

        # Display "WEEKLY TASKS" label
        weekly_tasks_label = tk.Label(self.weekly_tasks_frame, text="WEEKLY TASKS", font=("Helvetica", 20, "bold"),bg ="#00072D",fg ="#00FFFF")
        weekly_tasks_label.pack(pady=5)

        if selected_date >= start_of_week and selected_date <= end_of_week:
            if weekly_tasks:
                # Display weekly tasks as buttons in the frame
                for p, t, desc in weekly_tasks:
                    task_button = tk.Button(self.weekly_tasks_frame, text=f"Task: {t} | Priority: {p} | Description: {desc}",bg ="#00072D",fg ="#00FFFF",font=("Helvetica", 18))
                    task_button.pack(pady=5)
            else:
                # Display "No Tasks" label for weekly tasks
                no_tasks_label = tk.Label(self.weekly_tasks_frame, text="No tasks for the present week.",font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
                no_tasks_label.pack(pady=5)
        else:
            # Display "No Tasks" label for weekly tasks if not in the current week
            no_tasks_label = tk.Label(self.weekly_tasks_frame, text="No tasks for the present week.",font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
            no_tasks_label.pack(pady=5)

    def display_monthly_tasks(self):
        selected_date = self.full_calendar.get_date()
        today = datetime.now().date()
        start_of_month = today.replace(day=1)
        end_of_month = (start_of_month + timedelta(days=32)).replace(day=1) - timedelta(days=1)

        monthly_tasks = self.scheduler.task_queue_between_dates(start_of_month, end_of_month)

        # Clear previous monthly tasks
        for widget in self.monthly_tasks_frame.winfo_children():
            widget.destroy()

        # Display "MONTHLY TASKS" label
        monthly_tasks_label = tk.Label(self.monthly_tasks_frame, text="MONTHLY TASKS", font=("Helvetica", 20, "bold"),bg ="#00072D",fg ="#00FFFF")
        monthly_tasks_label.pack(pady=5)

        if selected_date >= start_of_month and selected_date <= end_of_month:
            if monthly_tasks:
                # Display monthly tasks as buttons in the frame
                for p, t, desc in monthly_tasks:
                    task_button = tk.Button(self.monthly_tasks_frame, text=f"Task: {t} | Priority: {p} | Description: {desc}",bg ="#00072D",fg ="#00FFFF",font=("Helvetica", 18))
                    task_button.pack(pady=5)
            else:
                # Display "No Tasks" label for monthly tasks
                no_tasks_label = tk.Label(self.monthly_tasks_frame, text="No tasks for the present month.",font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
                no_tasks_label.pack(pady=5)
        else:
            # Display "No Tasks" label for monthly tasks if not in the current month
            no_tasks_label = tk.Label(self.monthly_tasks_frame, text="No tasks for the present month.",font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
            no_tasks_label.pack(pady=5)

class TaskSchedulerUI:
    def __init__(self, master):
        self.scheduler = TaskScheduler()
        self.master = master
        self.master.title("Task Scheduler")
        self.master.attributes('-fullscreen', True)
        self.master.configure(bg = "#00072D")

        # Load the background image (subsample values adjusted for better visibility)
        bg = tk.PhotoImage(file=r"C:\Users\Laxmi Varshitha\OneDrive\Desktop\Scheduler\ds_pic2.png")
        
        bg = bg.subsample(2, 2)

        imb = tk.Label(self.master, image=bg,bg ="#00072D")
        imb.image = bg
        imb.place(x=0, y=0)

        # Left Side (Task Entry)
        self.left_frame = tk.Frame(master,bg ="#00072D")
        self.left_frame.grid(row=0, column=0, padx=40, pady=400)

        self.task_label = tk.Label(self.left_frame, text="Task:",font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
        self.task_label.grid(row=0, column=0, padx=5, pady=5)

        self.task_entry = tk.Entry(self.left_frame,font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
        self.task_entry.grid(row=0, column=1, padx=5, pady=5)

        self.priority_label = tk.Label(self.left_frame, text="Priority:",font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
        self.priority_label.grid(row=1, column=0, padx=5, pady=5)

        self.priority_entry = tk.Entry(self.left_frame,font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
        self.priority_entry.grid(row=1, column=1, padx=5, pady=5)

        self.description_label = tk.Label(self.left_frame, text="Description:",font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
        self.description_label.grid(row=2, column=0, padx=5, pady=5)

        self.description_entry = tk.Entry(self.left_frame,font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
        self.description_entry.grid(row=2, column=1, padx=5, pady=5)

        self.deadline_label = tk.Label(self.left_frame, text="Deadline:",font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
        self.deadline_label.grid(row=3, column=0, padx=5, pady=5)

        self.deadline_entry = DateEntry(self.left_frame, width=12, background='darkblue', foreground="#038298", borderwidth=2)
        self.deadline_entry.grid(row=3, column=1, padx=5, pady=5)

        self.add_button = tk.Button(self.left_frame, text="Add Task", command=self.add_task,font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
        self.add_button.grid(row=4, column=0, columnspan=2, pady=10)

        # New button to view tasks
        self.view_tasks_button = tk.Button(self.master, text="View Tasks", command=self.open_view_tasks_window,font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
        self.view_tasks_button.place(x = 400, y= 580)

        # Right Side (Display Tasks)
        self.right_frame = tk.Frame(master,bg = "#00072D")
        self.right_frame.place(x = 550, y =15)

        self.task_display_label = tk.Label(self.right_frame,bg ="#00072D")
        self.task_display_label.grid(row=0, column=0, padx=100, pady=0)

        # List to store references to task buttons
        self.task_buttons = []

        # Exit Button
        self.exit_button = tk.Button(self.master, text="X", command=self.exit_program, font=("Helvetica", 20),bg ="#00072D",fg ="#00FFFF")
        self.exit_button.place(x=1240, y=0)
       

    def open_view_tasks_window(self):
        view_tasks_root = tk.Toplevel(self.master)
        view_tasks_window = ViewTasksWindow(view_tasks_root, self.scheduler)
        self.master.configure(bg = "#00072D")
        

    def add_task(self):
        task = self.task_entry.get()
        priority = int(self.priority_entry.get())
        description = self.description_entry.get()
        deadline_date = self.deadline_entry.get_date()

        # If no date is selected, use the current date
        deadline_date = deadline_date if deadline_date else datetime.now().date()

        self.scheduler.add_task(task, priority, description, deadline_date)
        self.task_entry.delete(0, tk.END)
        self.priority_entry.delete(0, tk.END)
        self.description_entry.delete(0, tk.END)
        self.deadline_entry.set_date(None)
        self.display_tasks()
        messagebox.showinfo("Task Added", f"Task '{task}' added successfully!")

    def display_tasks(self):
        for button in self.task_buttons:
            button.destroy()
        self.task_buttons = []

        sorted_tasks = sorted(self.scheduler.task_queue, key=lambda x: x[0])

        task_buttons_frame = tk.Frame(self.right_frame,bg ="#00072D")
        task_buttons_frame.grid(row=1, column=0, padx=5, pady=5)

        for task_info in sorted_tasks:
            priority, task, description, time_added, deadline = task_info
            task_str = f"Task: {task} | Priority: {priority} | Description: {description} | Deadline: {deadline}"
            task_button = tk.Button(task_buttons_frame, text=task_str, command=lambda t=task: self.delete_task(t),font=("Helvetica", 18),bg ="#00072D",fg ="#00FFFF")
            task_button.pack(pady=5)
            self.task_buttons.append(task_button)

    def delete_task(self, task):
        try:
            self.scheduler.delete_task(task)
            self.task_entry.delete(0, tk.END)
            self.priority_entry.delete(0, tk.END)
            self.description_entry.delete(0, tk.END)
            self.deadline_entry.set_date(None)
            self.display_tasks()
            messagebox.showinfo("Task Deleted", f"Task '{task}' deleted successfully!")
        except ValueError:
            messagebox.showwarning("No Task Found", f"No task found with the name '{task}'.")

    def exit_program(self):
        self.master.destroy()

if __name__ == "__main__":
    intro_root = tk.Tk()
    intro = IntroductionWindow(intro_root)
    intro_root.mainloop()